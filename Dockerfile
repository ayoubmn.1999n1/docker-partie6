FROM tomcat
COPY projectDocker.war /usr/local/tomcat/webapps/projectDocker.war
COPY junit-4.12.jar  /usr/local/tomcat/junit.jar
COPY hamcrest-core-1.3.jar  /usr/local/tomcat/hamcrest.jar
COPY easymock-4.2.jar  /usr/local/tomcat/easymock.jar
COPY servlet.jar  /usr/local/tomcat/servlet.jar
COPY partie6.jar  /usr/local/tomcat/partie6.jar


